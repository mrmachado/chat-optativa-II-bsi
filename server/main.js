var app = require('http').createServer(handler)
var io = require('socket.io')(app);
let conn = {};

app.listen(80, function(){
  console.log('listening on *:80');
});

function handler(req, res) {
    res.writeHead(200);
    res.write('tô vivo!');
//    res.end(data);
    res.end();
}


io.on('connection', socket => {
	socket.emit('enter');

    socket.on('register', data => {
		conn[socket.id] = data.name;
		
		socket.broadcast.emit('xyz-client', {
            msg: data.name + ' entrou'
        });
		
        console.log(data.name + ' entrou. id: ' + socket.id)
    })
	
    socket.on('xyz-server', data => {
		console.log(data.name + ': ' + data.msg);
		
        socket.emit('xyz-client', {
            msg: 'Você: ' + data.msg
        });
		
		socket.broadcast.emit('xyz-client', {
            msg: data.name + ': ' + data.msg
        });
    });

    socket.on('disconnect', data => {
		name = conn[socket.id];
        console.log(name + ' saiu')
    })
});